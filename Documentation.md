# Documentation du TP CI/CD *Auteur : Louis Guilbaud Master-2 TSI 2019-2020* 

## Projet utilisé pour la création du pipeline  sources : https://testinfra.readthedocs.io/en/latest/
Testinfra est un outil pour écrire des tests unitaires en Python pour tester l'état réel de serveurs configurés par des outils de gestion tels que Salt, Ansible, Puppet, Chef, etc.

## Objectif du tp
L’objet du TP est de mettre en oeuvre de la livraison continue (“CD”) sur un projet.

Consignes à suivre : https://docs.google.com/document/d/1AtPzhCsW8xluITJSaSAqPFqPqhV54bwET__e3fIshHE/edit# 

## Expliquation du pipeline  ### 1) Plateforme de mise en ligne du code
La plateforme de mise en ligne de code choisi a été Gitlab : https://gitlab.com/LouisGuil/tp_cicd_pipeline.git. Depuis ce lien, il est possible de cloner le projet et de réaliser des pushs pour vérifier le pipeline. Le projet est en public pour que toutes les personnes voulant tester ou regarder la configuration du pipeline puissent le faire.

### 2) Logiciel de CD utilisé
Ensuite le logiciel de CD utilisé est celui intégré directement à Gitlab : Gilab CI/CD. En ayant regardé différent logiciel de CI/CD j'ai remarqué qu'il se ressemblait tous plus ou moins, j'ai pensé qu'il serait beaucoup utilisé et donc je pourrais trouver beaucoup d'aide sur internet en cas de problème. Aussi vu que mon code est déjà sur Gitlab j'ai pensé que cela faciliterait la configuration.

### 3) Configurations du pipeline
La configuration du pipeline se fait avec le fichier gitlab-ci.yml, répartis en 3 étapes: 
- Le build
- Les tests
- Le déploiement 
Toutes ces étapes utilisent une image python en version 3.7 ou (pour répondre à une des consignes).

En python il n’y a pas d’étapes “build” par défaut, par contre je peux "linter" le code. Cette pratique vise à améliorer la qualité du code. Cela veut dire que le linteur va s'attarder sur les fichiers sources du projet lorsque le code n'est pas exécuté. J'ai donc utilisé la librairie "tox" qui contient les librairies "lint" et "flake8" pour linter mon code dans l'étape de build.

Lors de l'étape du build, je construis la page web de la documentation grâce au "makefile" présent à la racine du projet. Celui-ci permet de "build" le site de documentation dans l'arborescence : doc/build/html. Ce code généré est ensuite placé dans un répertoire nommé "public". Grâce à l'outil "Gitlab page" il est possible de générer une page web static avec tous les fichiers contenue dans le dossier "public". L'argument "artefacts" puis "path" renseigne le dossier qui va contenir la page web static.

Si une branche est créée l'argument path sera : public + le nom de la branche afin d'avoir la documentation sur le master qui reste intact tout en observant les modifications sur sa branche. L'adresse de la documentation sera indiquée dans la console (https://gitlab.com/LouisGuil/tp_cicd_pipeline/nom_de_la_branche/html)

L'étape de test réalise des tests sur le code de l'application, voire s’il est fonctionnel et ne crée pas d'erreurs. Contrairement au "lint" ce test exécute le code lui-même et vérifie si une erreur se produit. J'ai aussi réalisé en parallèle un test, mais sur une ancienne version de python : 2.7, mais avec un attribut qui permet de continuer le pipeline si le test échoue. Enfin le dernier test vérifie que tous les fichiers sont présents dans le répertoire grâce au fichier check-manifest. Si un fichier est manquant, le test échoue.

L'étape de déploiement ne réalise rien si c'est qu'il indique le lien pour la documentation finale de l'outil comme lors de l'étape du build. Ce déploiement est réalisé uniquement sur la branche master grâce à l'argument "only: master".

Une dernière brique de code reste à expliquer : before_script. Dans cette brique sont renseignées les scriptes qui seront exécutées avant tous les autres scripts du fichier. Sont installé les librairies tox et bien sûr la librairie testinfra à chaque début de nouveau script.

**Runner utilisé :** les runners sont utilisées par les "jobs" pour exécuté les différents scripts. Pour mon pipeline je n'ai pas eu à réaliser de runner personnalisé, car tous ceux disponibles en open source ont pu exécuter mes scripts sans difficulté.

### 4) Remarques sur le rendu
La génération de documentation par branche est fonctionnelle, mais il est impossible d'ouvrir la page master plus la page d'une branche en même temps. En essayant de passer le nom de la branche dans l'url et en créant un dossier "public" pour chaque branche, cela n'a pas marché. Je pense que le problème provient de l'unique url que Gitlab page met à disposition par projet.

C'est le problème qui m'a pris le plus de temps à comprendre. Malgré la limite de temps largement dépassé, ce tp m'a permis de bien comprendre certaines subtilités du CD et surtout son utilité.